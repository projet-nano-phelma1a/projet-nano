#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

void destructeur(char **p, int n){
	int i;
	for(i=0;i<n;i++){
		free(p[i]);
	}
	free(p);
}

int resolution(int* tirage, int nb, int cible, int distancemin, char** operations)
{
    
	int i;
	int j;
	int k;
	int resultat;
	char buffer[50];

	int sauvetirage[24];
	int sauvetirage2[24];
	memcpy(sauvetirage, tirage, sizeof(int)*nb);
	
	char* hope="+-*/";
	for (i=0; i<nb-1; i++)
	{
		for (j=0; j<nb; j++ || j!=i)
		{
			
				for (k=0;k<4;k++)
				{
					char doublebreak=0;
                    memcpy(sauvetirage2, tirage, sizeof(int)*nb);
					switch(k){
						
						case 0:
							if(sauvetirage[i]==0 || sauvetirage[j]==0){
								doublebreak=1;
								break;
							}
							resultat=sauvetirage[i]+sauvetirage[j];
							break;

						case 1:
							if(sauvetirage[j]>sauvetirage[i] || sauvetirage[j]==0){
								doublebreak=1;
								break;
							}
							resultat=sauvetirage[i]-sauvetirage[j];
							break;
						case 2:
							if(sauvetirage[j]==0 || sauvetirage[j]==1 || sauvetirage[i]==0 || sauvetirage[i]==1){
								doublebreak=1;
								break;
							}
							resultat=sauvetirage[i]*sauvetirage[j];
							break;
						case 3:
							if(sauvetirage[j]==0 || sauvetirage[j]==1 || sauvetirage[i]==0){
								doublebreak=1;
								break;
							}
							
							resultat=sauvetirage[i]/sauvetirage[j];

							break;
					}

					if(doublebreak){continue;}
                    tirage[i]=resultat;
					tirage[j]=tirage[nb-1];
                    if(distancemin>cible-resultat){
                        distancemin=cible-resultat;
                        int taille=sprintf(buffer, "%d%c%d=%d", sauvetirage[i], hope[k], sauvetirage[j], resultat);
						operations[nb-2]=malloc((taille+1)*sizeof(char));
						strcpy(operations[nb-2], buffer);
						
						puts(buffer);
                        }
					
					if(resultat==cible || int val=resolution(tirage, nb-1, cible, distancemin, operations)==0){
						int taille=sprintf(buffer, "%d%c%d=%d", sauvetirage[i], hope[k], sauvetirage[j], resultat);
						operations[nb-2]=malloc((taille+1)*sizeof(char));
						strcpy(operations[nb-2], buffer);
                        puts(buffer);
						memcpy(tirage, sauvetirage2, sizeof(int)*nb);
						return 0;

					}
                    if(fasb(val)<fabs(distancemin)){
                        distancemin=val;
                        int taille=sprintf(buffer, "%d%c%d=%d", sauvetirage[i], hope[k], sauvetirage[j], resultat);
						operations[nb-2]=malloc((taille+1)*sizeof(char));
						strcpy(operations[nb-2], buffer);
						
                        puts(buffer);
                    }

                    
                    memcpy(tirage, sauvetirage2, sizeof(int)*nb);
					
					
				}
			
		}
	}
	return distancemin;
	destructeur(operations, nb);
	destructeur(sauvetirage, nb);
	destructeur(sauvetirage2, nb);
}

int main(int argc, char const *argv[])
{

	int tirage[] = {1,1,3,1,2,1};
    int cible=586;
	int distancemin=cible;
	int nb=6;
	
	char** operations=malloc(nb*sizeof(char*));
	resolution(tirage, nb, cible, distancemin, operations);
	
	return EXIT_SUCCESS;
	
}