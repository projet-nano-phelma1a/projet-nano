#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
//Pour free operations
void destructeur(char **p, int n){
	int i;
	for(i=0;i<n;i++){
		free(p[i]);
	}
	free(p);
}


	

//Resolution PB1
int resolution(int* tirage, int nb, int cible, char** operations)
{
	
	int i;
	int j;
	int k;
	int resultat;
	char buffer[50];
	int sauvetirage[24];
	memcpy(sauvetirage, tirage, sizeof(int)*nb);
	char* hope="+-*/";
	for (i=0; i<nb-1; i++)
	{
		for (j=0; j<nb; j++ && j!=i)
		{
			
				for (k=0;k<4;k++)
				{
					char doublebreak=0;
					switch(k){
						
						case 0:
							if(sauvetirage[i]==0 || sauvetirage[j]==0){
								doublebreak=1;
								break;
							}
							resultat=sauvetirage[i]+sauvetirage[j];
							break;

						case 1:
							if(sauvetirage[j]>sauvetirage[i] || sauvetirage[j]==0){
								doublebreak=1;
								break;
							}
							resultat=sauvetirage[i]-sauvetirage[j];
							break;
						case 2:
							if(sauvetirage[j]==0 || sauvetirage[j]==1 || sauvetirage[i]==0 || sauvetirage[i]==1){
								doublebreak=1;
								break;
							}
							resultat=sauvetirage[i]*sauvetirage[j];
							break;
						case 3:
							if(sauvetirage[j]==0 || sauvetirage[j]==1 || sauvetirage[i]==0 || sauvetirage[i]%sauvetirage[j]!=0){
								doublebreak=1;
								break;
							}
							
							resultat=sauvetirage[i]/sauvetirage[j];

							break;
					}

					if(doublebreak){continue;}
                    if(resultat)
					tirage[i]=resultat;
					tirage[j]=tirage[nb-1];
					if(resultat==cible || resolution(tirage, nb-1, cible, operations)==0){
						int taille=sprintf(buffer, "%d%c%d=%d", sauvetirage[i], hope[k], sauvetirage[j], resultat);
						operations[nb-2]=malloc((taille+1)*sizeof(char));
						strcpy(operations[nb-2], buffer);
						puts(buffer);
						
						
						
						return 0;

					}
	
				
				memcpy(tirage, sauvetirage, sizeof(int)*nb);

				}
			
		}
	}
	
	return 1;
	free(sauvetirage);
	

}

int main(int argc, char const *argv[])
{
	//Test rapide
	/*int tirage[] = {4,1,3,25,6,100};
    int cible=568;
	int nb=6;*/

	//Test avec lecture fichier
	int tirage[24];
	int nb;
	int cible;
	
	int i=0;
	int j=0;
	int number;
	char str[1000];
	int caract;
	int tr;
	printf("Entrez le nom du fichier de tirage :");
	scanf("%s", str);
	FILE* file = fopen (str, "r");

    fscanf (file, "%d", &nb);  
	
	while ((caract = fgetc(file))!=EOF)
		{  
		
		if(caract=='\n'){
			i+=1;
			
		}
		switch(i){
			case 0: break;
			case 1: 
				fscanf (file, "%d", &number);
				tirage[j]=number;
				j+=1;
				break;
			case 2:fscanf (file, "%d", &cible); break;
		}
	}
	fclose(file);

	//On réalise les opérations
	char** operations=malloc(nb*sizeof(char*));
	resolution(tirage, nb, cible, operations);

	destructeur(operations, nb);
	
	return EXIT_SUCCESS;
	
}
